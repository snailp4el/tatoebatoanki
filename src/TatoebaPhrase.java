/**
 * Created by petrushev on 02.12.2016.
 */
public class TatoebaPhrase {

    private String enPhrase;
    private String ruPhrase;
    private String ruPhrase2;
    private String numberOfPhrase;

    public TatoebaPhrase(String numberOfPhrase, String enPhrase, String ruPhrase, String ruPhrase2) {
        this.enPhrase = enPhrase;
        this.ruPhrase = ruPhrase;
        this.ruPhrase2 = ruPhrase2;
        this.numberOfPhrase = numberOfPhrase;
    }


    public TatoebaPhrase(String numberOfPhrase, String enPhrase, String ruPhrase) {
        this.enPhrase = enPhrase;
        this.ruPhrase = ruPhrase;
        this.numberOfPhrase = numberOfPhrase;
        System.out.println(numberOfPhrase+"==="+enPhrase+"==="+ruPhrase);
    }

    public String getEnPhrase() {
        return enPhrase;
    }

    public String getRuPhrase() {
        return ruPhrase;
    }

    public String getRuPhrase2() {
        return ruPhrase2;
    }

    public String getNumberOfPhrase() {
        return numberOfPhrase;
    }
}
