import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;

/**
 * Created by petrushev on 30.11.2016.
 */
public class SaveFileFromUrl {


    static public void saveMp3FromTateba(TatoebaPhrase tatoebaPhrase) {//only http pathAndName "D://anki//1111.mp3"
        //http://audio.tatoeba.org/sentences/eng/37902.mp3
        System.out.println(tatoebaPhrase.getNumberOfPhrase());
        String saveNumber = tatoebaPhrase.getNumberOfPhrase().replaceAll("[ru]", "");
        String http = "http://audio.tatoeba.org/sentences/eng/"+ saveNumber +".mp3";
        String pathAndName = "D://anki//" +tatoebaPhrase.getNumberOfPhrase()+ ".mp3";
        try {
            URLConnection conn = new URL(http).openConnection();
            InputStream is = conn.getInputStream();
            OutputStream outputStream = new FileOutputStream(new File(pathAndName));
            byte[] buffer = new byte[4096];
            int len;
            while ((len = is.read(buffer)) > 0) {
                outputStream.write(buffer, 0, len);
            }
            outputStream.close();

        }catch (IOException ioe){
            System.out.println("ERROR_saveMp3FromTatoeba" + ioe);
        }
    }






}
