import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;

public class URLJsopParser {



    public void grabThePageWithMp3 (String URL) {
        System.out.println("================URLJsopParser - grabThePageWithMp3");

        Document doc;

        try{
           doc  = Jsoup.connect(URL).get();
       }catch (IOException ioe){
           System.out.println("grabThePageWithMp3_doc");
           return;
        }

        Elements elements = doc.getElementsByClass("sentence-and-translations");


        for (Element e: elements) {

            Element audioElement = e.select("a").first();
            String number = audioElement.attr("href");
            number = number.replaceAll("[/eng/sentences/show/]", "");

            Elements phrasesElements = e.select("div.text");
            Element engPhraseElement = phrasesElements.get(0);
            String engPhrase = engPhraseElement.text();

            Element ruPhraseElement = phrasesElements.get(1);
            String ruPhrase = ruPhraseElement.text();

            TatoebaPhrase tatoebaPhrase = new TatoebaPhrase(number, engPhrase, ruPhrase);
            AnkiImportFileCreator.putPhraseToAnkiImportFile(tatoebaPhrase);
            SaveFileFromUrl.saveMp3FromTateba(tatoebaPhrase);



        }

    }


}
