
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.List;

public class AnkiImportFileCreator {


    static void putPhraseToAnkiImportFile(TatoebaPhrase tatoebaPhrase){

        List<String> lines;
        Path file = Paths.get("Anki.txt");

        //check file
        File f = new File(file.toAbsolutePath().toString());
        if(!f.exists()) {
            System.out.println("file "+ f +" dont exist");
            lines = Arrays.asList();
            try {
                Files.write(file, lines, Charset.forName("UTF-8"));
            } catch (IOException e) {
                System.out.println("putPhraseToAnkiImportFile write");
                e.printStackTrace();
            }
        }

        String toFile = tatoebaPhrase.getEnPhrase() +"="+ tatoebaPhrase.getRuPhrase() +"="+"[sound:" + tatoebaPhrase.getNumberOfPhrase()+".mp3]";

        lines = Arrays.asList(toFile);

        try {
            Files.write(file, lines, Charset.forName("UTF-8"), StandardOpenOption.APPEND);
        } catch (IOException e) {
            System.out.println("putPhraseToAnkiImportFile write2");
            e.printStackTrace();
        }

    }

}
